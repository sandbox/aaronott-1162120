
-- SUMMARY --

Roadblock allows you to create a one time landing page that is cookies 
controlled. It displays a chosen node once to all users, regardless of their 
point of entry to the site.

-- INSTALLATION --

* Install as usual, see 
  http://drupal.org/getting-started/5/install-contrib/modules for further 
  information.

-- CONFIGURATION --

* Once installed, a DB table will be auto-populated with user agents fetched 
  from http://www.user-agents.org. This allows the agents bypass the roadblock 
  page.

* Go to Administer -> Site Configuration -> Roadblock. Check the "Enable" box 
  to enable the module.

* Enter in your landing page path (e.g. node/#).

* Modify the Cookie life time to your needs.

* Optionally enable the Cron Job and specify the Interval time.

* Save Configuration.

* Optionally click "Refresh user agent database" to update the robots and 
  crawlers.

-- Additional Features in Development --

* Automatic (time-out) bypass with countdown

* Bypass code to access the site (Useful for newsletters)

* Multiple/random node as roadblock features

-- CREDITS & CONTACT --

Current maintainer:

Makara Wang - http://drupal.org/user/132402

This project has been sponsored by:

Raincity Studios - http://www.raincitystudios.com/
